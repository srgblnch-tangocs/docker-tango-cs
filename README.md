# [docker](https://www.docker.com/) frontend for [TANGO Control System](https://www.tango-controls.org/)

![4 - Beta](https://img.shields.io/badge/Development_Status-3_--_alpha-yellow.svg)

## Prepare

See the [docker-compose-tango-cs](https://gitlab.com/srgblnch-tangocs/docker-compose-tango-cs) 
project that uses this.

Docker image with tango-db service that has the database in a [backend 
database](https://gitlab.com/srgblnch-tangocs/docker-tango-cs-mariadb). See 
the readme there to create network dependency.

(TODO) Instead of the hardcoded password use docker secret

```bash
docker build -t tangodb-frontend:9.3.4 .
```

## Testing

(TODO): avoid hardcodings like the ip (first clean it in the backend)

```bash
docker container run -it \
    --network tango --ip 192.168.123.11 -p 10000:10000 \
    -e DATABASE_HOST=database:3306 \
    --name tango \
    tangodb-frontend:9.3.4
```

