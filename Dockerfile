# TANGO Control System Dockerfile

#FROM debian:stretch
FROM debian:buster
MAINTAINER TANGO Controls Team <info@tango-controls.org>

RUN apt-get update \
 && apt-get install -y \
    supervisor \
    dbconfig-mysql \
    # dependencies in debian9
#    libcos4-1 \
#    libmariadbclient18 \
#    libomniorb4-1 \
#    libomnithread3c2 \
#    openjdk-8-jre \
    # dependencies in debian 10
    libcos4-2 \
    libmariadb3 \
    libomniorb4-2 \
    libomnithread4 \
    openjdk-11-jre \
    # debendencies in debian
    libzmq5 \
    liblog4j1.2-java \
    ;

# in case use the tango from the distribution
#    tango-common \
#    tango-db \
#    tango-starter \
#    tango-test \
#    tango-accesscontrol

COPY dpkg/preseed.txt /tmp

#COPY dpkg/bpo9 /tmp
#CMD export DEBIAN_FRONTEND=noninteractive; cd /tmp; dpkg -i \
#    tango-common_9.3.4+dfsg1-2~bpo9+1+salsaci_all.deb \
#    liblog4tango5v5_9.3.4+dfsg1-2~bpo9+1+salsaci_amd64.deb \
#    libtango9_9.3.4+dfsg1-2~bpo9+1+salsaci_amd64.deb \
#    libtango-tools_9.3.4+dfsg1-2~bpo9+1+salsaci_amd64.deb \
#    tango-db_9.3.4+dfsg1-2~bpo9+1+salsaci_amd64.deb \
#    #libtango-java_9.3.4+ci2-1~bpo9+1+salsaci_all.deb \
#    tango-starter_9.3.4+dfsg1-2~bpo9+1+salsaci_amd64.deb \
#    tango-accesscontrol_9.3.4+dfsg1-2~bpo9+1+salsaci_amd64.deb \
#    tango-test_9.3.4+dfsg1-2~bpo9+1+salsaci_amd64.deb && \
#    rm /tmp/*deb \
#    ; debconf-set-selections preseed.txt
COPY dpkg/bpo10 /tmp
RUN export DEBIAN_FRONTEND=noninteractive; cd /tmp; dpkg -i \
    tango-common_9.3.4+48.93a21d-2~bpo10+1+salsaci_all.deb \
    liblog4tango5v5_9.3.4+48.93a21d-2~bpo10+1+salsaci_amd64.deb \
    libtango9_9.3.4+48.93a21d-2~bpo10+1+salsaci_amd64.deb \
    libtango-tools_9.3.4+48.93a21d-2~bpo10+1+salsaci_amd64.deb \
    tango-db_9.3.4+48.93a21d-2~bpo10+1+salsaci_amd64.deb \
    libtango-java_9.3.4+ci2-1~bpo10+1+salsaci_all.deb \
    tango-starter_9.3.4+48.93a21d-2~bpo10+1+salsaci_amd64.deb \
    tango-accesscontrol_9.3.4+48.93a21d-2~bpo10+1+salsaci_amd64.deb \
    tango-test_9.3.4+48.93a21d-2~bpo10+1+salsaci_amd64.deb && \
    rm /tmp/*deb \
    ; debconf-set-selections preseed.txt

COPY resources/tango_register_device /usr/local/bin/
COPY resources/wait-for-it.sh        /usr/local/bin/
COPY resources/supervisord.conf      /etc/supervisord.conf

# used by tango-db to know where the database is
ENV MYSQL_HOST=database
ENV MYSQL_USER=tango
ENV MYSQL_PASSWORD=(...)

ENV LD_LIBRARY_PATH=/usr/local/lib
ENV ORB_PORT=10000
ENV TANGO_HOST=localhost:${ORB_PORT}
RUN echo TANGO_HOST=$TANGO_HOST > /etc/tangorc

EXPOSE ${ORB_PORT}

#RUN useradd -ms /bin/bash tangosys
#USER tangosys

CMD /usr/local/bin/wait-for-it.sh $DATABASE_HOST --timeout=30 --strict -- \
    /usr/bin/supervisord -c /etc/supervisord.conf
